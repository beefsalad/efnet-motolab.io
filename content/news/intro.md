Title: New website on GitLab Pages
Date: 2017-07-16

The site is now on GitLab Pages.  You can contribute by submitting a merge request at <https://gitlab.com/efnet-moto/efnet-moto.gitlab.io>.
